﻿using System;
using System.Text;
using video_bot.DAL;
using video_bot.DAL.Models;

namespace video_bot.Parser
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;

            var parser = new ChParser();

            var config = new ChParserSettings {
                BaseUrl = "https://coursehunters.net/categories",
                StartPoint = 1
            };

            parser.Parse(config);

            Console.WriteLine("Process finished!");

            Console.ReadKey();
        }
    }
}

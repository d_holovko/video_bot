﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Text;

namespace video_bot.Parser
{
    public interface IParser
    {
        void Parse(IParserSettings settings);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace video_bot.Parser
{
    public interface IParserSettings
    {
        string BaseUrl { get; set; }

        string Prefix { get; set; }

        int StartPoint { get; set; }

        int EndPoint { get; set; }
    }
}

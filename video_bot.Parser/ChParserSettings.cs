﻿using System;
using System.Collections.Generic;
using System.Text;

namespace video_bot.Parser
{
    public class ChParserSettings : IParserSettings
    {
        public string BaseUrl { get; set; } = "https://coursehunters.net";

        public string Prefix { get; set; } = "page";

        public int StartPoint { get; set; }

        public int EndPoint { get; set; }
    }
}

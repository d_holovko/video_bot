﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using HtmlAgilityPack;
using video_bot.DAL;
using video_bot.DAL.Models;

namespace video_bot.Parser
{
    public class ChParser : IParser
    {
        private readonly IRepository<Category> repository = new BaseRepository<Category>();

        private HtmlWeb web = new HtmlWeb();

        public void Parse(IParserSettings settings)
        {
            var html = web.Load(settings.BaseUrl);

            int counter = 1;
            foreach (var categoryAnchor in html.DocumentNode.SelectNodes("/html/body/div[1]/div[2]/div/div/a"))
            {
                if (counter++ < settings.StartPoint)
                {
                    continue;
                }

                var title = categoryAnchor.ChildNodes.FindFirst("h2").InnerText;

                var imageLink = categoryAnchor.ChildNodes.FindFirst("picture")
                        .ChildNodes.FindFirst("img").Attributes["src"].Value;

                var config = GeneratePageSettings(categoryAnchor.Attributes["href"].Value);

                Console.WriteLine($"category: {title}");

                var category = ExtractCategoryData(config);

                category.Title = title;
                category.ImageLink = imageLink;

                var res = repository.Insert(category);

                if (!res.IsSuccessful)
                {
                    Console.WriteLine($"Could not save {title}");
                    throw new OperationCanceledException(res.Message);
                }

                Thread.Sleep(1000);
            }
        }

        public Category ExtractCategoryData(IParserSettings settings)
        {
            var category = new Category();

            var courses = new List<Course>();

            for (int i = settings.StartPoint; i <= settings.EndPoint; i++)
            {
                Console.WriteLine($"page: {i}");

                var html = web.Load($"{settings.BaseUrl}?{settings.Prefix}={i}");

                category.Description = html.DocumentNode.SelectSingleNode("/html/body/div[1]/div[2]/div[2]/p").InnerText;

                var courseArticles = html.DocumentNode.SelectNodes("/html/body/div[1]/div[2]/section/div[1]/article")
                    .Where(x => x.Attributes["title"] == null)
                    .ToArray();

                foreach (var courseArticle in courseArticles)
                {
                    var link = courseArticle.ChildNodes.First(x => x.HasClass("standard-course-block__course-name"))
                            .ChildNodes.FindFirst("a").Attributes["href"].Value;

                    var title = courseArticle.ChildNodes.First(x => x.HasClass("standard-course-block__course-name"))
                            .ChildNodes.FindFirst("a")
                            .ChildNodes.FindFirst("h3").InnerText;

                    var source = courseArticle.ChildNodes.Last(x => x.HasClass("standard-course-block__course-name"))
                            .ChildNodes.FindFirst("a").InnerText;

                    var language = courseArticle.ChildNodes
                            .First(x => x.HasClass("standard-course-block__course-lang")).InnerText;

                    var duration = courseArticle.ChildNodes
                            .First(x => x.HasClass("standard-course-block__duration"))
                            .InnerText.Replace("Duration ", string.Empty);

                    Console.WriteLine($"course: {title}");

                    try
                    {
                        var course = ExtractCourseData(link);

                        course.Title = title;
                        course.Source = source;
                        course.Language = language;
                        course.Duration = duration;

                        courses.Add(course);
                    }
                    catch (Exception exception)
                    {
                        Console.WriteLine($"Parsing of course \"{title}\" caused throubles:");
                        Console.WriteLine(exception.Message);
                    }                    

                    Thread.Sleep(1000);
                }

                Thread.Sleep(1000);
            }

            category.Courses = courses;
            category.CoursesAmount = courses.Count;

            return category;
        }

        public Course ExtractCourseData(string url)
        {
            var course = new Course();

            var html = web.Load(url);

            var description = html.DocumentNode.SelectSingleNode("/html/body/div[1]/div[2]/div[2]/div/article")
                .ChildNodes.Last(x => x.HasClass("standard-block")).InnerText;

            var videosData = html.DocumentNode
                .SelectNodes("/html/body/div[1]/div[2]/div[2]/div/article/div[2]/details/ul/li")
                .Select(x => new Lesson {
                    Title = x.ChildNodes.FindFirst("span").InnerText,
                    Duration = x.ChildNodes.First(t => t.HasClass("lessons-list__duration")).InnerText,
                    Url = x.ChildNodes.First(t =>
                        t.Attributes["itemprop"] != null && t.Attributes["itemprop"].Value == "contentUrl")
                        .Attributes["href"].Value
                }).ToArray();

            course.Description = description.ToString();
            course.Lessons = videosData;
            course.LessonsAmount = videosData.Count();

            return course;
        }

        public IParserSettings GeneratePageSettings(string url)
        {
            var html = web.Load(url);

            var settings = new ChParserSettings { BaseUrl = url, StartPoint = 1 };

            var paging = html.DocumentNode.SelectNodes("/html/body/div[1]/div[2]/section[2]/div[2]/ul/li/a/span");

            if (paging == null)
            {
                settings.EndPoint = 1;

                return settings;
            }

            settings.EndPoint = int.Parse(paging.Select(x => x.InnerText).Reverse().ToArray()[1]);

            return settings;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using video_bot.DAL.Models;

namespace video_bot.DAL.OperationResults
{
    public class DatabaseOperationResult
    {
        public bool IsSuccessful { get; set; }

        public string Message { get; set; }
    }

    public class DeleteOperationResult : DatabaseOperationResult
    {
    }

    public class DeleteAllOperationResult : DatabaseOperationResult
    {
    }

    public class DeleteByIdOperationResult : DatabaseOperationResult
    {
    }

    public class GetByIdOperationResult<TEntity> : DatabaseOperationResult
    where TEntity : class, IEntity
    {
        public TEntity Entity { get; set; }
    }

    public class GetOperationResult<TEntity> : DatabaseOperationResult
    where TEntity : class, IEntity
    {
        public GetOperationResult()
        {
            Entities = new List<TEntity>();
        }

        public IEnumerable<TEntity> Entities { get; set; }
    }

    public class GetAllOperationResult<TEntity> : GetOperationResult<TEntity>
    where TEntity : class, IEntity
    {

    }

    public class GetByFieldOperationResult<TEntity> : GetOperationResult<TEntity>
    where TEntity : class, IEntity
    {

    }

    public class InsertOperationResult : DatabaseOperationResult
    {
    }

    public class UpdateOperationResult : DatabaseOperationResult
    {
    }
}

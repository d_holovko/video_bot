﻿using Microsoft.Extensions.Configuration;
using Mongo.CRUD;
using Mongo.CRUD.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Linq;
using video_bot.DAL.Models;
using video_bot.DAL.OperationResults;

namespace video_bot.DAL
{
    public class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : class, IEntity, new()
    {
        private IConfigurationRoot configuration;

        private const string DATABASE__DATABASE_CONNECTION_STRING = "mongoConnection";
        private const string DATABASE__DATABASE_NAME = "mongoDbName";

        private const string MESSAGES__NOT_FOUND_MESSAGE = "notFoundMessage";
        private const string MESSAGES__EMPTY_COLLECTION_MESSAGE = "emptyCollectionMessage";

        private const string MESSAGES__DELETE_ALL_OPERATION_SUCCESS_MESSAGE = "deleteAllOperationSuccessMessage";
        private const string MESSAGES__DELETE_ALL_OPERATION_FAIL_MESSAGE = "deleteAllOperationFailMessage";
        private const string MESSAGES__DELETE_ALL_OPERATION_EXCEPTION_MESSAGE = "deleteAllOperationExceptionMessage";

        private const string MESSAGES__DELETE_BY_ID_OPERATION_SUCCESS_MESSAGE = "deleteByIdOperationSuccessMessage";
        private const string MESSAGES__DELETE_BY_ID_OPERATION_FAIL_MESSAGE = "deleteByIdOperationFailMessage";
        private const string MESSAGES__DELETE_BY_ID_OPERATION_EXCEPTION_MESSAGE = "deleteByIdOperationExceptionMessage";

        private const string MESSAGES__GET_OPERATION_SUCCESS_MESSAGE = "getOperationSuccessMessage";
        private const string MESSAGES__GET_OPERATION_EXCEPTION_MESSAGE = "getOperationExeptionMessage";

        private const string MESSAGES__GET_All_OPERATION_SUCCESS_MESSAGE = "getAllOperationSuccessMessage";
        private const string MESSAGES__GET_ALL_OPERATION_EXCEPTION_MESSAGE = "getAllOperationExceptionMessage";

        private const string MESSAGES__GET_BY_FIELD_OPERATION_SUCCESS_MESSAGE = "getByFieldOperationSuccessMessage";
        private const string MESSAGES__GET_BY_FIELD_OPERATION_EXCEPTION_MESSAGE = "getByFieldOperationExceptionMessage";

        private const string MESSAGES__GET_BY_ID_OPERATION_SUCCESS_MESSAGE = "getByIdOperationSuccessMessage";
        private const string MESSAGES__GET_BY_ID_OPERATION_FAIL_MESSAGE = "getByIdOperationFailMessage";
        private const string MESSAGES__GET_BY_ID_OPERATION_EXCEPTION_MESSAGE = "getByIdOperationExceptionMessage";

        private const string MESSAGES__INSERT_OPERATION_SUCCESS_MESSAGE = "insertOperationSuccessMessage";
        private const string MESSAGES__INSERT_OPERATION_EXCEPTION_MESSAGE = "insertOperationExceptionMessage";

        private const string MESSAGES__UPDATE_OPERATION_SUCCESS_MESSAGE = "updateOperationSuccessMessage";
        private const string MESSAGES__UPDATE_OPERATION_EXCEPTION_MESSAGE = "updateOperationExceptionMessage";


        private readonly IMongoCRUD<TEntity> client;

        public BaseRepository()
        {
            var builder = new ConfigurationBuilder();
            builder.AddJsonFile("config.json");
            configuration = builder.Build();

            Console.WriteLine(configuration.GetSection(MESSAGES__NOT_FOUND_MESSAGE).Value);

            client = new MongoCRUD<TEntity>(
                configuration.GetSection(DATABASE__DATABASE_CONNECTION_STRING).Value,
                configuration.GetSection(DATABASE__DATABASE_NAME).Value
            );
        }

        public virtual DeleteAllOperationResult DeleteAll()
        {
            var result = new DeleteAllOperationResult();

            try
            {
                var filter = Builders<TEntity>.Filter.Empty;
                result.IsSuccessful = client.DeleteByQuery(filter);

                result.Message = result.IsSuccessful
                ? MESSAGES__DELETE_ALL_OPERATION_SUCCESS_MESSAGE
                : MESSAGES__DELETE_ALL_OPERATION_FAIL_MESSAGE;

                return result;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                result.IsSuccessful = false;
                result.Message = MESSAGES__DELETE_ALL_OPERATION_EXCEPTION_MESSAGE;
                throw;
            }
        }

        public virtual DeleteByIdOperationResult DeleteById(ObjectId id)
        {
            if (id == default(ObjectId))
            {
                throw new ArgumentException(nameof(id));
            }

            var result = new DeleteByIdOperationResult();

            try
            {
                result.IsSuccessful = client.Delete(id);

                result.Message = result.IsSuccessful
                ? MESSAGES__DELETE_BY_ID_OPERATION_SUCCESS_MESSAGE
                : MESSAGES__DELETE_BY_ID_OPERATION_FAIL_MESSAGE;

                return result;
            }
            catch (ArgumentException exception)
            {
                Console.WriteLine(exception.Message);
                result.IsSuccessful = false;
                result.Message = MESSAGES__DELETE_BY_ID_OPERATION_EXCEPTION_MESSAGE;
                throw;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                result.IsSuccessful = false;
                result.Message = MESSAGES__DELETE_BY_ID_OPERATION_EXCEPTION_MESSAGE;
                throw;
            }
        }

        public virtual GetOperationResult<TEntity> Get(FilterDefinition<TEntity> filter, SearchOptions options)
        {
            if (filter == null)
            {
                throw new ArgumentNullException(nameof(filter));
            }

            if (options == null)
            {
                throw new ArgumentNullException(nameof(options));
            }

            var result = new GetOperationResult<TEntity>();

            try
            {
                result.Entities = client.Search(filter, options).Documents;

                if (!result.Entities.Any())
                {
                    result.IsSuccessful = false;
                    result.Message = MESSAGES__NOT_FOUND_MESSAGE;

                    return result;
                }

                result.IsSuccessful = true;
                result.Message = MESSAGES__GET_OPERATION_SUCCESS_MESSAGE;

                return result;
            }
            catch (ArgumentNullException exception)
            {
                Console.WriteLine(exception.Message);
                result.IsSuccessful = false;
                result.Message = MESSAGES__GET_OPERATION_EXCEPTION_MESSAGE;
                return result;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                result.IsSuccessful = false;
                result.Message = MESSAGES__GET_OPERATION_EXCEPTION_MESSAGE;
                return result;
            }
        }

        public virtual GetAllOperationResult<TEntity> GetAll()
        {
            var result = new GetAllOperationResult<TEntity>();

            try
            {
                var filter = Builders<TEntity>.Filter.Empty;

                result.Entities = client.Search(filter).Documents;

                if (!result.Entities.Any())
                {
                    result.IsSuccessful = false;
                    result.Message = MESSAGES__EMPTY_COLLECTION_MESSAGE;

                    return result;
                }

                result.IsSuccessful = true;
                result.Message = MESSAGES__GET_All_OPERATION_SUCCESS_MESSAGE;

                return result;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                result.IsSuccessful = false;
                result.Message = MESSAGES__GET_ALL_OPERATION_EXCEPTION_MESSAGE;
                throw;
            }
        }

        public virtual GetByFieldOperationResult<TEntity> GetByField(string fieldName, string fieldValue)
        {
            if (string.IsNullOrWhiteSpace(fieldName))
            {
                throw new ArgumentException(nameof(fieldName));
            }

            var result = new GetByFieldOperationResult<TEntity>();

            try
            {
                var filter = Builders<TEntity>.Filter.Eq(fieldName, fieldValue);

                result.Entities = client.Search(filter).Documents;

                if (!result.Entities.Any())
                {
                    result.IsSuccessful = false;
                    result.Message = MESSAGES__NOT_FOUND_MESSAGE;

                    return result;
                }

                result.IsSuccessful = true;
                result.Message = MESSAGES__GET_BY_FIELD_OPERATION_SUCCESS_MESSAGE;

                return result;
            }
            catch (ArgumentException exception)
            {
                Console.WriteLine(exception.Message);
                result.IsSuccessful = false;
                result.Message = MESSAGES__GET_BY_FIELD_OPERATION_EXCEPTION_MESSAGE;
                throw;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                result.IsSuccessful = false;
                result.Message = MESSAGES__GET_BY_FIELD_OPERATION_EXCEPTION_MESSAGE;
                throw;
            }
        }

        public virtual GetByIdOperationResult<TEntity> GetById(ObjectId id)
        {
            if (id == default(ObjectId))
            {
                throw new ArgumentException(nameof(id));
            }

            var result = new GetByIdOperationResult<TEntity>();

            try
            {
                result.Entity = client.Get(id);

                if (result.Entity == null)
                {
                    result.IsSuccessful = false;
                    result.Message = MESSAGES__NOT_FOUND_MESSAGE;

                    return result;
                }

                result.IsSuccessful = true;
                result.Message = result.IsSuccessful
                ? MESSAGES__GET_BY_ID_OPERATION_SUCCESS_MESSAGE
                : MESSAGES__GET_BY_ID_OPERATION_FAIL_MESSAGE;

                return result;
            }
            catch (ArgumentException exception)
            {
                Console.WriteLine(exception.Message);
                result.IsSuccessful = false;
                result.Message = MESSAGES__GET_BY_ID_OPERATION_EXCEPTION_MESSAGE;
                throw;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                result.IsSuccessful = false;
                result.Message = MESSAGES__GET_BY_ID_OPERATION_EXCEPTION_MESSAGE;
                throw;
            }
        }

        public virtual InsertOperationResult Insert(TEntity entity)
        {
            var result = new InsertOperationResult();

            try
            {
                client.Create(entity);

                result.IsSuccessful = true;
                result.Message = MESSAGES__INSERT_OPERATION_SUCCESS_MESSAGE;

                return result;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                result.IsSuccessful = false;
                result.Message = MESSAGES__INSERT_OPERATION_EXCEPTION_MESSAGE;
                throw;
            }
        }

        public virtual UpdateOperationResult Update(TEntity entity)
        {
            var result = new UpdateOperationResult();

            try
            {
                client.Update(entity);

                result.IsSuccessful = true;
                result.Message = MESSAGES__UPDATE_OPERATION_SUCCESS_MESSAGE;

                return result;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                result.IsSuccessful = false;
                result.Message = MESSAGES__UPDATE_OPERATION_EXCEPTION_MESSAGE;
                throw;
            }
        }
    }
}

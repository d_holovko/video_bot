﻿using Mongo.CRUD.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using video_bot.DAL.Models;
using video_bot.DAL.OperationResults;

namespace video_bot.DAL
{
    public interface IRepository<TEntity> where TEntity : class, IEntity, new()
    {
        DeleteAllOperationResult DeleteAll();

        DeleteByIdOperationResult DeleteById(ObjectId id);

        GetOperationResult<TEntity> Get(FilterDefinition<TEntity> filter, SearchOptions options);

        GetAllOperationResult<TEntity> GetAll();

        GetByIdOperationResult<TEntity> GetById(ObjectId id);

        GetByFieldOperationResult<TEntity> GetByField(string fieldName, string fieldValue);

        InsertOperationResult Insert(TEntity entity);

        UpdateOperationResult Update(TEntity entity);
    }    
}

﻿using Mongo.CRUD.Attributes;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace video_bot.DAL.Models
{
    public interface IEntity
    {
        [BsonId]
        ObjectId Id { get; set; }
    }

    public class BaseEntity : IEntity
    {
        [BsonId]
        public ObjectId Id { get; set; }
    }

    [CollectionName("Courses")]
    public class Category : BaseEntity
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public string ImageLink { get; set; }

        public int CoursesAmount { get; set; }
        
        public IEnumerable<Course> Courses { get; set; }
    }

    public class Course
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public string Source { get; set; }

        public string Language { get; set; }

        public int LessonsAmount { get; set; }

        public string Duration { get; set; }

        public IEnumerable<Lesson> Lessons { get; set; }
    }

    public class Lesson
    {
        public string Title { get; set; }

        public string Url { get; set; }

        public string Duration { get; set; }
    }
}
